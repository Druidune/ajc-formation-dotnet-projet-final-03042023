﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace menu.api
{
    public class OptionMenu
    {
        public int chiffreId;
        public string description;
        public ISousInterface classe;

        public OptionMenu(int chiffreId, string description, ISousInterface classe)
        {
            this.chiffreId = chiffreId;
            this.description = description;
            this.classe = classe;
        }
    }
}
