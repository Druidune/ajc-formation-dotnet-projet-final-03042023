﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace menu.api
{
    public class MenuPrincipal
    {
        List<OptionMenu> listeOptionMenu = new List<OptionMenu>();


        public void AjoutOptionMenu(OptionMenu optionMenu)
        {
            this.listeOptionMenu.Add(optionMenu);
        }

        public void Afficher()
        {
            foreach(var option in this.listeOptionMenu)
            {
                Console.WriteLine($"{option.chiffreId}. {option.description}");
            }
            var choixUtilisateur = Console.ReadLine();

            foreach(var option in this.listeOptionMenu)
            {
                if (choixUtilisateur == option.chiffreId.ToString())
                {
                    option.classe.Afficher();
                }
            }
        }
        public MenuPrincipal() { }

    }
}
