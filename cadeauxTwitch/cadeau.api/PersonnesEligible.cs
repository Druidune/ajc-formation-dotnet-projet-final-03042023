﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cadeau.api
{
    public class PersonnesEligible
    {
        private List<Personne> listPersonnes = new List<Personne>();
        private int pourcentageMin = 70;

        public PersonnesEligible() { }
        public PersonnesEligible(List<Personne> listPersonnes) 
        {
            this.Personnes = listPersonnes;
        }

        public void AjoutPersonne(Personne personne, DateTime débutStream)
        {
            if (this.personneFidele(personne, débutStream))
            {
                this.listPersonnes.Add(personne);
            }
        }

        private bool personneFidele(Personne personne, DateTime d)
        {
            var pourcentageTempsPasse = personne.TempsPasse * 100 / d.Minute;

            if (pourcentageTempsPasse > this.pourcentageMin)
            {
                return true;
            } else 
            { 
                return false; 
            }
        }

        public List<Personne> Personnes { get => listPersonnes; set => listPersonnes = value; }
    }
}
