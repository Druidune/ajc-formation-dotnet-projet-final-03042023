﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cadeau.api
{
    public class StreamTwitch
    {
        private DateTime débutStream = DateTime.Now;

        public StreamTwitch() { }
        public StreamTwitch(DateTime débutStream)
        {
            this.DébutStream = débutStream;
        }

        public DateTime DébutStream { get => débutStream; set => débutStream = value; }
    }
}
