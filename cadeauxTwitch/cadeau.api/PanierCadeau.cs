﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cadeau.api
{
    public class PanierCadeau
    {
        private List<Cadeau> panier = new List<Cadeau>();

        public List<Cadeau> Panier { get => panier; set => panier = value; }

        public PanierCadeau() { }

        public void AjoutCadeau(Cadeau nouveauCadeau)
        {
            this.panier.Add(nouveauCadeau);
        }
		
	}
}
