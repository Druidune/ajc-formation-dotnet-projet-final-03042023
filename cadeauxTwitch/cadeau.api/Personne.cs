﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cadeau.api
{
    public class Personne
    {
        private string prenom;
        private string surnom;
        private string photoDeProfil;
        private string email;
        private int tempsPasse;
        private bool estConnecte;

        public string Prenom { get => prenom; set => prenom = value; }
        public string Email { get => email; set => email = value; }
        public int TempsPasse { get => tempsPasse; set => tempsPasse = value; }
        public bool EstConnecte { get => estConnecte; set => estConnecte = value; }
        public string Surnom { get => surnom; set => surnom = value; }
        public string PhotoDeProfil { get => photoDeProfil; set => photoDeProfil = value; }

        public Personne() { }
        public Personne(string prenom, string email, int tempsPasse, bool connecte)
        {
            Prenom = prenom;
            Email = email;
            TempsPasse = tempsPasse;
            EstConnecte = connecte;

        }
    }
}
