﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cadeau.api
{
    public class Cadeau
    {
        private string nom;
        private string type;
        private string description;

        public string Nom { get => nom; set => nom = value; }
        public string Type { get => type; set => type = value; }
        public string Description { get => description; set => description = value; }

        public Cadeau() { }
        public Cadeau(string nom, string type)
        {
            this.nom = nom;
        }
        public Cadeau(string nom, string type, string description)
        {
            this.nom = nom;
            this.description = description;
        }
    }
}
