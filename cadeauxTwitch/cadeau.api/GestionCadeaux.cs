﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cadeau.api
{
    public class GestionCadeaux
    {
        private PersonnesEligible personneFidele;
        private PanierCadeau listCadeau;
        private Random r = new Random();

        public GestionCadeaux() { }
        public GestionCadeaux(PersonnesEligible personneFidele, PanierCadeau listCadeau)
        {
            this.PersonneFidele = personneFidele;
            this.ListCadeau = listCadeau;
        }

        public void tirageAuSort()
        {
            if (this.personneFidele.Personnes.Count > 0 && this.listCadeau.Panier.Count > 0) 
            {
                Personne gagnant = this.personneFidele.Personnes[r.Next(0, this.personneFidele.Personnes.Count)];
                Cadeau gagné = this.listCadeau.Panier[r.Next(0, this.listCadeau.Panier.Count)];

                Console.WriteLine($"Le/La gagnant(e) est {gagnant.Prenom} ({gagnant.Email}) ! Il/Elle reçoit {gagné.Nom} ({gagné.Description})");
            }
        }

        public PersonnesEligible PersonneFidele { get => personneFidele; set => personneFidele = value; }
        public PanierCadeau ListCadeau { get => listCadeau; set => listCadeau = value; }
    }
}
