﻿using cadeau.api;
using menu.api;

StreamTwitch stream = new StreamTwitch(DateTime.Now);

List<Cadeau> cadeaux = new List<Cadeau>() {
new Cadeau("beyblade", "toupie", "toupie"),
new Cadeau("beyblade", "toupie", "toupie"),
new Cadeau("beyblade", "toupie", "toupie"),
new Cadeau("beyblade", "toupie", "toupie") };



List<Personne> personnes = new List<Personne>() {
    new Personne("jsp", "jsp@gmail.com", 61, true),
new Personne("fjfj", "jsp@gmail.com", 61, true),
new Personne("frfe", "jsp@gmail.com", 61, true),
new Personne("fefe", "jsp@gmail.com", 61, true),
new Personne("jefsp", "jsp@gmail.com", 61, true),
new Personne("jsfep", "jsp@gmail.com", 61, true),
new Personne("jgrvegfnéioezfnsp", "jsp@gmail.com", 61, true),
new Personne("jkfébfeuzibfusp", "jsp@gmail.com", 61, true) };

PersonnesEligible listPersonne = new PersonnesEligible();
PanierCadeau panierCadeau = new PanierCadeau();

foreach (Cadeau cadeau in cadeaux)
{
    panierCadeau.AjoutCadeau(cadeau);
}

foreach (Personne person in personnes)
{
    listPersonne.AjoutPersonne(person, stream.DébutStream);
}

GestionCadeaux gestion = new GestionCadeaux(listPersonne, panierCadeau);
//gestion.tirageAuSort();

MenuPrincipal menu = new MenuPrincipal();

AjoutPersonne ajoutPersonne = new AjoutPersonne();
ListerCadeaux listerCadeaux = new ListerCadeaux();

menu.AjoutOptionMenu(new OptionMenu(1, "Ajouter une personne", ajoutPersonne));
menu.AjoutOptionMenu(new OptionMenu(2, "lister les cadeaux présents", listerCadeaux));
menu.AjoutOptionMenu(new OptionMenu(3, "Ajouter un cadeau", ajoutPersonne));

menu.Afficher();

